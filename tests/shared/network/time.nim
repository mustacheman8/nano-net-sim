import
    std/[unittest, random],
    ./../../../shared/network/time

suite "Coordinator":

    setup:
        let coordinator = newCoordinator()

    suite "addTimer()":

        test "should activate timer":
            var startingValue = 0
            let expected = rand(1000)

            coordinator.addTimer(proc() = startingValue = expected, 1)

            coordinator.tick()
            check startingValue == expected
        
        test "should only activate timer after duration has passed":
            var startingValue = 0
            let expected = rand(1000)

            coordinator.addTimer(proc() = startingValue = expected, 2)

            coordinator.tick()
            check startingValue == startingValue

            coordinator.tick()
            check startingValue == expected

        test "should activate timers in order":
            var startingValue = 0
            let expected = rand(1000)
            let expected2 = rand(1000)

            coordinator.addTimer(proc() = startingValue = expected2, 2)
            coordinator.addTimer(proc() = startingValue = expected, 1)

            coordinator.tick()
            check startingValue == expected

            coordinator.tick()
            check startingValue == expected2

        test "should activate each timer only once":
            var startingValue = 0

            coordinator.addTimer(proc() = startingValue += 1, 1)

            for i in 1..1000:
                coordinator.tick()

            check startingValue == 1

        
