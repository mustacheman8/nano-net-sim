import
  macros,
  math

macro `|>`*(lhs, rhs: untyped): untyped =
  case rhs.kind:
  of nnkIdent: # single-parameter functions
    result = newCall(rhs, lhs)
  else:
    result = rhs
    result.insert(1, lhs)

proc exp*(s: SomeInteger, f: float64, T: typedesc[SomeFloat] = float): T =
  T(s).pow(f)