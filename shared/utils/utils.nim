import
  random,
  sequtils

proc pureShuffle*[T](sequence: seq[T]): seq[T] =
  var indices = toSeq(0..(sequence.len()-1))
  indices.shuffle()
  for i in indices:
    result.add(sequence[i])

proc all*[T](sequence: seq[T], function: proc): bool =
  for item in sequence:
    if not function(item):
      return false
  return true

proc take*[T](sequence: seq[T], count: int): seq[T] =
  for i in 0..(count-1):
    result.add(sequence[i])

proc skip*[T](sequence: seq[T], count: int): seq[T] =
  for i in count..sequence.len():
    result.add(sequence[i]) 

