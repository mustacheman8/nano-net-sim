type
    Timer = ref object
        callback: proc(): void
        startAt, endAt: int
    Coordinator* = ref object
        time: int
        timers: seq[Timer]
        longTimers: seq[Timer]
        veryLongTimers: seq[Timer]
        extremelyLongTimers: seq[Timer]

#[
    Coordinator
]#

const longTime = 6
const veryLongTime = 24
const extremelyLongTime = 72

proc getTime*(c: Coordinator): int =
    c.time

proc addTimer*(c: Coordinator, p: proc(), duration: int) =
    if duration > extremelyLongTime:
        c.extremelyLongTimers.add(Timer(callback: p, startAt: c.time, endAt: c.time + duration))
    elif duration > veryLongTime:
        c.veryLongTimers.add(Timer(callback: p, startAt: c.time, endAt: c.time + duration))
    elif duration > longTime:
        c.longTimers.add(Timer(callback: p, startAt: c.time, endAt: c.time + duration))
    else:    
        c.timers.add(Timer(callback: p, startAt: c.time, endAt: c.time + duration))

proc expireExtremelyLongTimers(c: Coordinator): seq[Timer] =
    var tempTimers = c.extremelyLongTimers
    for t in tempTimers:
        if t.endAt < c.time + extremelyLongTime:
            result.add(t)

proc unexpiredExtremelyLongTimers(c: Coordinator): seq[Timer] =
    for t in c.extremelyLongTimers:
        if t.endAt >= c.time + extremelyLongTime:
            result.add(t)

proc expireVeryLongTimers(c: Coordinator): seq[Timer] =
    var tempTimers = c.veryLongTimers
    for t in tempTimers:
        if t.endAt < c.time + veryLongTime:
            result.add(t)

proc unexpiredVeryLongTimers(c: Coordinator): seq[Timer] =
    for t in c.veryLongTimers:
        if t.endAt >= c.time + veryLongTime:
            result.add(t)

proc expireLongTimers(c: Coordinator): seq[Timer] =
    var tempTimers = c.longTimers
    for t in tempTimers:
        if t.endAt < c.time + longTime:
            result.add(t)

proc unexpiredLongTimers(c: Coordinator): seq[Timer] =
    for t in c.longTimers:
        if t.endAt >= c.time + longTime:
            result.add(t)

proc expireTimers(c: Coordinator) =
    var tempTimers = c.timers
    for t in tempTimers:
        if t.endAt == c.time:
            t.callback()

proc unexpiredTimers(c: Coordinator): seq[Timer] =
    for t in c.timers:
        if t.endAt > c.time:
            result.add(t)

proc tick*(c: Coordinator) =
    c.time += 1
    if c.time mod extremelyLongTime == 0:
        c.veryLongTimers.add(c.expireExtremelyLongTimers())
        c.extremelyLongTimers = c.unexpiredExtremelyLongTimers()
    if c.time mod veryLongTime == 0:
        c.longTimers.add(c.expireVeryLongTimers())
        c.veryLongTimers = c.unexpiredVeryLongTimers()
    if c.time mod longTime == 0:
        c.timers.add(c.expireLongTimers())
        c.longTimers = c.unexpiredLongTimers()
    c.expireTimers()
    c.timers = c.unexpiredTimers()

func newCoordinator*: Coordinator =
    Coordinator(time: 0, timers: newSeq[Timer]())
    