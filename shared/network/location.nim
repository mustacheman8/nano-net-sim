import
    std/[math, random],
    ../utils/ops

type Location* = object
    x*: int
    y*: int

#[
    Location
]#

proc newLocation*: Location =
    Location(x: rand(1..300), y: rand(1..300))

proc distance*(origin, destination: Location): int =
    let xdif = abs(origin.x - destination.x)^2
    let ydif = abs(origin.y - destination.y)^2
    (xdif + ydif)   |>
    float           |>
    math.pow(0.5)   |>
    math.ceil()     |>
    int