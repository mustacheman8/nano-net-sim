import
    id

type
    Message* = ref object of RootObj
        sender: Id
    Transaction* = ref object of Message
        id*: Id
        confirmed*: bool
        creationTime*: int
        confirmedTime*: int    

#[
    Transaction
]#

proc `==`*(lhs, rhs: Transaction): bool =
    lhs.id == rhs.id

proc insert*(sequence: var seq[Transaction], transaction: Transaction) =
    for i, e in sequence:
        if e.id == transaction.id:
            sequence[i] = transaction
            return
    sequence.add(transaction)

func within*(transaction: Transaction, sequence: seq[Transaction]): bool =
    for e in sequence:
        if e == transaction:
            return true
    return false

proc absent*(transaction: Transaction, sequence: seq[Transaction]): bool =
    not transaction.within(sequence)
    
func newTransaction*(id: Id, creationTime: int): Transaction =
    Transaction(id: id, confirmed: false, creationTime: creationTime, confirmedTime: -1)