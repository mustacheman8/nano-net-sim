type
    Id* = distinct int16
    IdGenerator* = ref object
        latest: Id

#[
    Id
]#

proc `+`*(a: Id, b: int): Id = Id(int16(a) + b)
proc `==`*(a: Id, b: Id): bool = int16(a) == int16(b)
proc `$`*(a: Id): string = $(int16(a))


#[
    IdGenerator
]#

proc next*(g: IdGenerator): Id =
    g.latest = g.latest + 1
    result = g.latest

