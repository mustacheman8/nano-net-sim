import
    std/[sequtils, sugar, random, lists],
    message, time, location, id

type
    PeerChoice* = tuple[id: Id, distance: int]
    NetworkNode* = ref object of RootObj
        coordinator*: Coordinator
        network*: Network
        location*: Location
        dataQueue*: DoublyLinkedList[Message]
        id*: Id
        bandwidthUsed*: int
    Network* = ref object of RootObj
        nodes*: seq[NetworkNode]
    SubNetwork* = ref object of Network
        maxSize*: int
        network: Network

#[
    Network
]#

func newNetwork*: Network =
    Network(nodes: newSeq[NetworkNode]())

method findPeers*(network: Network, n: NetworkNode): seq[NetworkNode] {.base.} =
    network.nodes.filter(node => not(n == node))

method addNode*(n: Network, node: NetworkNode) {.base.} =
    n.nodes.add(node)

method randomNode*(n: Network): NetworkNode {.base.} =
    sample(n.nodes)

#[
    SubNetwork
]#

func newSubNetwork*(maxSize: int): SubNetwork =
    SubNetwork(nodes: newSeq[NetworkNode](), maxSize: maxSize)

proc superNet*(s: SubNetwork): Network =
    if s.network of SubNetwork:
        superNet(SubNetwork(s.network))
    else:
        s.network

#[
    NetworkNode
]#

func newNetworkNode*(c: Coordinator, n: Network, l: Location = newLocation(), id: Id): NetworkNode =
    NetworkNode(
        coordinator: c,
        network: n,
        location: l,
        id: id,
        dataQueue: DoublyLinkedList[Message](),
        bandwidthUsed: 0)

method receive*(receiver: NetworkNode, m: Message) {.base.} =
    receiver.bandwidthUsed += 1
    receiver.dataQueue.append(m)

method send*(sender, receiver: NetworkNode, m: Message) {.base.} =
    sender.bandwidthUsed += 1
    sender.coordinator.addTimer(
        proc () = receiver.receive(m),
        distance(sender.location, receiver.location)
    )

method broadcast*(sender: NetworkNode, m: Message) {.base.} =
    for receiver in sender.network.nodes:
        if receiver != sender:
            sender.send(receiver, m)

