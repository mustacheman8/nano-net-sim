import
    std/[random, sugar, times, strutils, lenientops],
    shared/network/[id, time, network, message, location],
    nano/[node, votingNode]

randomize()

# the maximum number of ticks the simulation will run for
let maxSimulationLength = 250000
# the number of VotingNanoNodes (PRs) in the simulation
let votingNodeCount = 10
# must not exceed ~65,000 because there is an ID limit of 65,535
let goalConfirmationCount = 5000
# if this is too high for nodes to handle, CPS will drop and sim speed can dramatically fall off as well
var transactionsPublishedPerSecond: int = 200
let publishLimit = 25000
# the number of nodes that must agree on a transaction before it is confirmed
let quorum = int((votingNodeCount * 2) div 3 + 1)
# The amount of time VotingNanoNodes wait between each processing step. Trades accuracy for simulation speed
let processingPause = 48
# The "processing power" of a voting node. Affects the rate of election processing.
let processingFactor = 80
let processingPower = processingFactor * processingPause
# The bandwidth limit for each voting node. A factor of 1 is roughly 2 mbps but varies by node count
let bandwidthFactor = 80
let bandwidthLimit = bandwidthFactor * processingPause

let myCoordinator = newCoordinator()
let myNetwork = newNetwork()
let idGenerator = IdGenerator()
let myWatcher = newNanoNode(myCoordinator, myNetwork, quorum, id = idGenerator.next(), l = Location(x: 0, y: 0))
myCoordinator.addTimer(proc () = myWatcher.nanoNodeTick(1), 1)

var myNodes = newSeq[VotingNanoNode]()

for i in 0..(votingNodeCount-1):
    capture i:
        myNodes.add(newVotingNanoNode(
            myCoordinator, myNetwork, quorum = quorum, id = idGenerator.next(), processingPower = processingPower, bandwidthLimit = bandwidthLimit))
        myCoordinator.addTimer(proc () = myNodes[i].nanoNodeTick(processingPause), processingPause)

for n in myNodes:
    n.updatePeerChoices()
    n.updatePeerList()
    n.addWatcher(myWatcher)

var votingCompletionTime = 0

let startTime = now()
var lastTime = now()
var lastSimTime = 0
var nextSendTime = 0f
var transactions = newSeq[Transaction]()
var transactionsSent = 0
var confirmations = newSeq[Transaction]()
var lastConfirmationCount = 0

let windowLength = 350
var lastWindow = 0
var windowConfirmations = 0
var maxBPS = 0
var windowStallCount = 0
let windowStallLimit = 7

for time in 0..(maxSimulationLength - 1):
    if (now() - lastTime).inMilliseconds > 500f:
        write(stdout, "\n")
        write(stdout, "Simspeed: ", formatFloat(float(time - lastSimTime) * 1000 / ((now() - lastTime).inMilliseconds), ffDecimal, 1), "   \n")
        write(stdout, "time: ", time, " / ", maxSimulationLength, "\n")
        write(stdout, "trx: ", confirmations.len(), " / ", transactionsSent, "\n")
        write(stdout, "bps: ", transactionsPublishedPerSecond, "   \r\e[4A")
        lastTime = now()
        lastSimTime = time
        lastConfirmationCount = confirmations.len()
    myCoordinator.tick()
    while time > nextSendTime and transactions.len() < publishLimit:
        let transaction = newTransaction(idGenerator.next(), myCoordinator.getTime())
        transactions.add(transaction)
        myWatcher.broadcast(transaction)
        transactionsSent += 1
        nextSendTime += 1000 / transactionsPublishedPerSecond
    for transaction in transactions:
        if not transaction.within(confirmations):
            if myWatcher.ledgerContains(transaction):
                transaction.confirmedTime = time
                confirmations.add(transaction)
                windowConfirmations += 1
    if confirmations.len() >= goalConfirmationCount:
        votingCompletionTime = time
        break
    if time > (lastWindow + windowLength):
        if (windowConfirmations * 1000 / windowLength) > transactionsPublishedPerSecond * 0.99:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 1.08 + 1)
            if transactionsPublishedPerSecond > maxBPS:
                maxBPS = transactionsPublishedPerSecond
        elif (windowConfirmations * 1000 / windowLength) > transactionsPublishedPerSecond * 0.93:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 1.05 + 1)
            if transactionsPublishedPerSecond > maxBPS:
                maxBPS = transactionsPublishedPerSecond
        elif (windowConfirmations * 1000 / windowLength) > transactionsPublishedPerSecond * 0.87 - 1:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 1.03 + 1)
            if transactionsPublishedPerSecond > maxBPS:
                maxBPS = transactionsPublishedPerSecond
        elif (windowConfirmations * 1000 / windowLength) < transactionsPublishedPerSecond * 0.86:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 0.98 - 1)
        elif (windowConfirmations * 1000 / windowLength) < transactionsPublishedPerSecond * 0.82:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 0.95 - 1)
        elif (windowConfirmations * 1000 / windowLength) < transactionsPublishedPerSecond * 0.78:
            transactionsPublishedPerSecond = int(transactionsPublishedPerSecond * 0.91 - 1)
        if maxBPS > transactionsPublishedPerSecond:
            windowStallCount += 1
            if windowStallCount > windowStallLimit:
                votingCompletionTime = time
                break
        else:
            windowStallCount = 0
        lastWindow = time
        windowConfirmations = 0

let ticksPerSecond = (votingCompletionTime * 1000) div ((now() - startTime).inMilliseconds + 1)

write(stdout, "\n\n\n\n\n")
echo("=========================================================================")
echo("        Id      Bandwidth       Mbps    Elections       Xcoord  Ycoord")
for n in myNodes:
    echo("\t", n.id, "\t", n.bandwidthUsed,"\t\t", formatFloat(1160 / 1000000 * n.bandwidthUsed / votingCompletionTime * 1000, ffDecimal, 2), "\t", n.workProcessed,"\t\t", n.location.x,"\t",n.location.y)
echo("=========================================================================")
var totalBandwidthUsed = 0
for n in myNodes:
    totalBandwidthUsed += n.bandwidthUsed
var totalElections = 0
for n in myNodes:
    totalElections += n.workProcessed    
echo("      mean","\t", totalBandwidthUsed /% myNodes.len(), "\t\t\t", totalElections /% myNodes.len())
echo("Simulation ended after ", votingCompletionTime, " ticks (simspeed: ", ticksPerSecond," ticks/s)")
echo("Confirmations completed: ", confirmations.len(), " / ", transactions.len())
echo("Highest BPS reached: ", maxBPS)