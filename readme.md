### The purpose of this simulator
is to quickly probe the effects of various network topologies/overlays on bandwidth and latency.  For this reason, Node behavior is faked.  Each task processed by a Node, regardless of complexity, is presumed to be completed at the same rate.  However, this processing rate may be changed on a per-node basis to simulate nodes of various processing capabilities.

### A brief description of how it works
Currently the main.nim file creates a single non-voting Node that broadcasts transactions as messages to the Voting Nodes.  Each Node is given a random x, y coordinate location.  Messages require time, dependent on distance between Nodes, to reach their destinations.  Upon receiving a message, the Node places the message in a queue to await processing.  During the processing step, Voting Nodes add their signatures to the transaction and broadcast again.  A transaction with 67% of all Voting Node signatures is considered confirmed and may be cemented in the ledger.

### Q/A
**What is "tick rate?"**

The term "tick rate" is used to represent the passage of time. Where calculation of rates (e.g. BPS, CPS) are necessary, 1 tick is treated as 1 millisecond.



### To run the program, use this command:
```nim c -r ./main.nim```

##### For a much faster simulation, use the release option:
```nim c -d:release -r ./main.nim```

##### Try the capacity test to find a reasonable BPS that a given network can sustain:
```nim c -d:release -r ./capacity.nim```

##### CPU profiling:
import nimprof in main.nim

```nim c --profiler:on --stackTrace:on -r ./main.nim```

##### Memory profiling:
import nimprof in main.nim

```nim c --profiler:off --stackTrace:on -d:memProfiler -r ./main.nim```

##### Test suite:
run all tests:   ```testament all```

run specific tests example:  ```testament r shared/network/time.nim```