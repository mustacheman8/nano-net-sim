import 
    std/[math, algorithm, lists, tables, enumerate, random, sequtils],
    ../shared/network/[network, location, message, time, id],
    election, node

type
    VotingNanoNode* = ref object of NanoNode
        watchers: seq[NanoNode]
        workProcessed*: int
        peers: seq[VotingNanoNode]
        peerChoices: seq[PeerChoice]
        elections*: seq[TimedElection]
        electionIndex: int
        lastBandwidthMeasurement: int
        bandwidthLimit: int

#[
    VotingNanoNode
]#

proc addWatcher*(n: VotingNanoNode, watcher: NanoNode) =
    n.watchers.add(watcher)

proc informWatchers*(n: VotingNanoNode, m: Message) =
    for watcher in n.watchers:
        n.send(watcher, m)

method broadcastToVoters*(n: VotingNanoNode, election: TimedElection) {.base.} =
    var copy = election
    for p in n.peers:
        n.send(p, copy)

method broadcastToSqrtVoters*(n: VotingNanoNode, election: TimedElection) {.base.} =
    var copy = election
    var indices = toSeq(0..(n.peers.len() - 1))
    indices.shuffle()
    var randomSubset = indices[0 ..< n.peers.len.float32.sqrt.ceil.int]
    for i in randomSubset:
        n.send(n.peers[i], copy)

method broadcastToMissingVoters(n: VotingNanoNode, election: TimedElection) {.base.} =
    var copy = election
    for p in n.peers:
        if ({p.id} - copy.signatures) == {p.id}:
            n.send(p, copy)

method broadcastToSqrtMissingVoters*(n: VotingNanoNode, election: TimedElection) {.base.} =
    var copy = election
    var sendCount = 0
    var indices = toSeq(0..(n.peers.len() - 1))
    indices.shuffle()
    for i in indices:
        if n.peers[i].id in copy.signatures:
            continue
        n.send(n.peers[i], copy)
        sendCount += 1
        if sendCount >= (n.peers.len.float32.sqrt.ceil.int + 1):
            break

proc deduplicateCombiningSignatures(n: VotingNanoNode, e: var TimedElection): TimedElection =
    var index = 0
    var current: TimedElection
    while index < (n.elections.len() - 1):
        current = n.elections[index]
        if current != e:
            index += 1
            continue
        else:
            e.signatures = e.signatures + current.signatures
            n.elections.delete(index)
    return e

method processDataQueue(n: VotingNanoNode, processingTime: int): int =
    var processed = 0
    while processed < processingTime and not isNil(n.dataQueue.head):
        processed += 1

        var message = n.dataQueue.head.value
        n.dataQueue.remove(n.dataQueue.head)

        if message of Transaction:
            let t = Transaction(message)
            if t.id notin n.ledger:
                n.elections.insert(newTimedElection(t, n.coordinator))
        elif message of TimedElection:
            let e = TimedElection(message)
            if e.transaction.id notin n.ledger:
                n.elections.add(e)
    return processingTime - processed

method processElections(n: VotingNanoNode, remainingProcessingTime: int) =
    let processingTime = remainingProcessingTime div 50
    var processed = 0
    while processed < processingTime and n.elections.len() > 0:
        if n.electionIndex >= n.elections.len():
            n.electionIndex = 0
        processed += 1
        n.workProcessed += 1

        var nextElection = n.elections[n.electionIndex]
        n.electionIndex += 1

        # transaction isn't in ledger
        if nextElection.transaction.id notin n.ledger:
            let election = deduplicateCombiningSignatures(n, nextElection)
            let transaction = election.transaction
            # this node hasn't voted, so vote
            if {n.id} - election.signatures == {n.id}:
                election.signatures = election.signatures + {n.id}
            # bandwidth limit check
            if (n.bandwidthUsed - n.lastBandwidthMeasurement) > n.bandwidthLimit:
                break
            # election confirmed; add to ledger and inform watchers
            if election.isPassing(n.quorum):
                n.ledger[transaction.id] = transaction
                transaction.confirmed = true
                n.informWatchers(transaction)
            n.broadcastToVoters(election)

method nanoNodeTick*(n: VotingNanoNode, tickRate: int) =
    var remainingProcessingTime = n.processDataQueue(n.processingPower)
    n.processElections(remainingProcessingTime)
    n.lastBandwidthMeasurement = n.bandwidthUsed
    n.coordinator.addTimer(proc () = n.nanoNodeTick(tickRate), tickRate)

func newVotingNanoNode*(c: Coordinator, n: Network, quorum: int, l: Location = newLocation(), id: Id, processingPower: int, bandwidthLimit: int): VotingNanoNode =
    result = VotingNanoNode(
        coordinator: c,
        network: n,
        location: l,
        id: id,
        dataQueue: DoublyLinkedList[Message](),
        ledger: newTable[Id, Transaction](),
        bandwidthUsed: 0,
        workProcessed: 0,
        quorum: quorum,
        watchers: newSeq[NanoNode](),
        peers: newSeq[VotingNanoNode](),
        peerChoices: newSeq[PeerChoice](),
        elections: newSeq[TimedElection](),
        electionIndex: 0,
        processingPower: processingPower,
        bandwidthLimit: bandwidthLimit,
    )
    n.addNode(result)
    return result

proc peerCmp(a, b: PeerChoice): int =
    if a.distance < b.distance: -1
    elif a.distance == b.distance: 0
    else: 1

proc updatePeerChoices*(n: VotingNanoNode) =
    var peerChoices = newSeq[PeerChoice]()
    for i, node in n.network.nodes:
        if node != n:
            let choice: PeerChoice = (id: node.id, distance: distance(n.location, node.location))
            peerChoices.add(choice)
    peerChoices.sort(peerCmp)
    n.peerChoices = peerChoices

method updatePeerList*(n: VotingNanoNode) {.base.} =
    var newPeers = newSeq[VotingNanoNode]()
    for pc in n.peerChoices:
        for node in n.network.nodes:
            if node of VotingNanoNode and (node.id == pc.id):
                newPeers.add(VotingNanoNode(node))
    n.peers = newPeers