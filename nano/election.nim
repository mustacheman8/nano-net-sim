import 
    std/lists,
    ../shared/network/[message, time, id]

type
    Election = ref object of Message
        transaction*: Transaction
        signatures*: set[Id]
    TimedElection* = ref object of Election
        timeToLive: int
        coordinator: Coordinator
        lastVote: int

#[
    Election
]#

proc `==`*(lhs, rhs: Election): bool =
    lhs.transaction.id == rhs.transaction.id

func newElection*(t: Transaction): Election = 
    Election(transaction: t, signatures: {})

proc isPassing*(e: Election, requiredSignatureCount: int): bool =
    e.signatures.card() >= requiredSignatureCount

proc retrieveSignatures*(storage: DoublyLinkedList[Election], election: Election): set[Id] =
    for e in storage:
        if e.transaction == election.transaction:
            return e.signatures
    return {}

proc insert*(storage: var DoublyLinkedList[Election], election: Election) =
    for i in mitems(storage):
        if i.transaction == election.transaction:
            i = election
            return
    storage.append(election)

#[
    TimedElection
]#

proc `==`*(lhs, rhs: TimedElection): bool =
    lhs.transaction.id == rhs.transaction.id

proc retrieveSignatures*(storage: DoublyLinkedList[TimedElection], election: TimedElection): set[Id] =
    for e in storage:
        if e.transaction == election.transaction:
            return e.signatures
    return {}

proc insert*(storage: var DoublyLinkedList[TimedElection], election: TimedElection) =
    # add signatures to existing election
    for i in mitems(storage):
        if i.transaction == election.transaction:
            i.signatures = election.signatures
            return
    storage.append(election)

func newTimedElection*(t: Transaction, c: Coordinator): TimedElection =
    TimedElection(
        transaction: t,
        signatures: {},
        lastVote: 0,
    )