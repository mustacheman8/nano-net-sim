import 
    std/[lists, tables],
    ../shared/network/[network, location, message, time, id],
    election

type 
    NanoNode* = ref object of NetworkNode
        processingPower*: int
        newBlocks*: seq[TimedElection]
        ledger*: TableRef[Id, Transaction]
        quorum*: int

#[
    NanoNode
]#

proc ledgerContains*(n: NanoNode, transaction: Transaction): bool =
    transaction.id in n.ledger

method processDataQueue(n: NanoNode) {.base.} =
    while true:
        if n.dataQueue.head == nil:
            break
        var message = n.dataQueue.head.value
        n.dataQueue.remove(n.dataQueue.head)
        if message of Transaction:
            let t = Transaction(message)
            if (t.id notin n.ledger) and t.confirmed:
                n.ledger[t.id] = t

method nanoNodeTick*(n: NanoNode, tickRate: int) {.base.} =
    n.processDataQueue()
    n.coordinator.addTimer(proc () = n.nanoNodeTick(tickRate), tickRate)

func newNanoNode*(c: Coordinator, n: Network, quorum: int, l: Location = newLocation(), id: Id): NanoNode =
    result = NanoNode(
        coordinator: c,
        network: n,
        location: l,
        id: id,
        quorum: quorum,
        dataQueue: DoublyLinkedList[Message](),
        ledger: newTable[Id, Transaction](),
        bandwidthUsed: 0,
        processingPower: 0
    )

